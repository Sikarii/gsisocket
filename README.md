# GSI-Socket (CS:GO)

Program that acts a server for Gamestate Integration and forwards the data via sockets.

### Notes
- Gamestate Integration listener is avaiable @ port 4000 by default
- Gamestate Integration socket server is available @ port 4001 by default

### Requirements
- `gamestate_integration_gsisocket.cfg` in `/csgo/cfg/` (See example config below).


### Example config
```

"GSISocket"
{
 	"uri" "http://localhost:4000"
 	"timeout" "5.0"
 	"buffer"  "0.1"
 	"throttle" "0.1"
 	"heartbeat" "30.0"
 	"data"
 	{
   		"provider"            "1"
   		"map"                 "1"
   		"round"               "1"
   		"player_id"           "1"
   		"player_state"        "1"
 	}
}
```
