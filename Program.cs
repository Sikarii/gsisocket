using GSISocket.Socket;

namespace GSISocket
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var gsi = new GSISocketServer();
            gsi.Start();
            gsi.SetupSockets();
        }
    }
}
